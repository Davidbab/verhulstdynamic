"""
A small interactive Simulation of the Verlust-dynamic. The User can change
the start parameters with two sliders underneath the Plot.

Bugs, Ideas and Tips to dizzelowski@gmx.de
"""

import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import numpy as np

def iterate(x0, w, n):
    """iterates the population with given parameters"""

    x = np.zeros(n, dtype=np.float64)        #array for population-values
    x[0] = np.float64(x0)               #initial population
    for i in range(n-1):
        x[i+1] = x[i]*w*(1-x[i])
    return x

def updatex(val):
    global x0
    x0 = slidex.val
    update(x0, w, n)

def updatew(val):
    global w
    w = slidew.val
    update(x0, w, n)

def update(x0, w, n):
    plot.set_ydata(iterate(x0, w, n))


x0 = 0.05    #startpopulation
w = 1.5      #grow-parameter
n = 100      #number of calculated iterations

plt.subplot(111, title="Verhulst-Dynamik")
plot, = plt.plot( np.linspace(0,n,n), iterate(x0,w,n) )
plt.xlim(0,n)
plt.ylim(0,1)
plt.subplots_adjust(bottom=0.25)
axisx = plt.axes([0.2, 0.1, 0.7, 0.03])
axisw = plt.axes([0.2, 0.15, 0.7, 0.03])
slidex = Slider(axisx, "Startpopulation", 0, 1, valinit=x0, valfmt="%1.2f")
slidew = Slider(axisw, "Wachstumsrate", 0, 4, valinit=w, valfmt="%1.1f")
slidex.on_changed(updatex)
slidew.on_changed(updatew)
plt.show()
